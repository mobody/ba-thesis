%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Second Chapter *********************************
%*******************************************************************************

\chapter{Grundlagen}
\label{chap:grundlagen}

\ifpdf
    \graphicspath{{Chapter2/Figs/Raster/}{Chapter2/Figs/PDF/}{Chapter2/Figs/}}
\else
    \graphicspath{{Chapter2/Figs/Vector/}{Chapter2/Figs/}}
\fi

\section{Influenza}
\label{sec:influenza}

Die Bezeichnungen Influenza und Grippe kamen gleichzeitig im Jahre 1743 auf. Zu dieser Zeit herrschte eine gewaltige Influenza-Epidemie in Europa, welche in Werken verschiedener Autoren Beschreibung fand.\citef{gluge37} Die Ausbreitung erstreckte sich über ganz Europa, welches von Osten nach Westen von der Epidemie durchzogen wurde. Influenza wurde die Krankheit zum ersten Mal von dem englischen Arzt Huxham genannt.\citef{huxham90} Das Wort ist vom lateinischen Verb \textit{influere} (hineingiessen, beeinflussen) abgeleitet; es deutet auf die Annahme hin, daß die Kälte Einfluss auf die Entstehung hat.
Das Wort Grippe kam in Frankreich auf. Es wird von \textit{gripper} (erwischen, erhaschen) abgeleitet und hat wahrscheinlich den selben Stamm wie das deutsche Wort greifen.\citef{ruhemann14}

\subsection{Krankheitsverlauf und Vorkommen} % (fold)
\label{sub:krankheitsverlauf}

Der regelmäßig aktualisierte Ratgeber des Robert Koch Institutes (RKI) für die Erkrankung saisonaler Influenzaviren beschreibt die Symptomatik wie folgt: "Die Influenza-typische Symptomatik ist durch plötzlichen Erkrankungsbeginn, Fieber, Husten oder Halsschmerzen sowie Muskel- und/oder Kopfschmerzen gekennzeichnet. Weitere Symptome können allgemeine Schwäche, Schweißausbrüche, Rhinorrhö, selten auch Übelkeit/Erbrechen und Durchfall sein."\citef{RKI16} Es wird jedoch geschätzt, dass die Hälfte aller Infizierten keine klinischen Symptome oder Zeichen aufweisen. Der Krankheitsverlauf der anderen Hälfte, kann sich zwischen fieberfreien Atemwegserkrankungen und den oben beschriebenen Symptomen abzeichnen.\citef{hoffmann16} Daher wird die Grippe oft auch mit einer einfachen Erkältung (grippaler Infekt) verwechselt (siehe Tabelle~\ref{tab:influenza}). 

\begin{table}[ht]
\begin{center}
\begin{small}
\resizebox{\textwidth}{!}{\begin{tabular}{l | l | l}
\toprule
Symptom 								& Influenza 					& Erkältung \\
\midrule 
Fieber 									& Üblicherweise hoch, 3-4 Tage 	& Unüblich \\
Kopfschmerzen 							& Ja 						   	& Unüblich \\
Schwächegefühl 							& Bis zu 2-3 Wochen				& Nicht stark ausgeprägt \\
Schmerzen 								& Üblich und häufig stark		& Schwach ausgeprägt \\
Erschöpfung 							& Früh und manchmal stark		& Nein \\
Verstopfte Nase 						& Manchmal 						& Üblich \\
Wunder Rachen 							& Manchmal 						& Üblich \\
Husten 									& Ja 							& Unüblich \\
Unbehaglichkeitsgefühl in der Brust 	& Üblich und häufig stark 		& Nicht stark bis moderat ausgeprägt \\
Komplikationen 							& Bronchitis, Lungenentzündung  & Nasen\-neben\-höhlen\-entzündung \\
\bottomrule
\end{tabular}}
\end{small}
\end{center}
\caption{Unterschiede zwischen Influenza und Erkältung}
\label{tab:influenza}
\end{table}
% subsection krankheitsverlauf (end)

Im Gegensatz zur Grippe, kann eine Erkältung durch 30 verschiedene Erreger hervorgerufen werden.\citef{turner11} Allein durch die Symptome kann eine Unterscheidung oft nicht vorgenommen werden. Eine Influenza-Erkrankung kann vor allem bei älteren Menschen, chronisch Kranken und Schwangeren zu Komplikationen wie beispielsweise einer Lungenentzündung führen und dann sogar tödlich verlaufen. Eine genauere Einteilung in Risikogruppen gibt die Schutzimpfungs-Richtlinie des Gemeinsamen Bundesausschusses.\citef{sirl15} Hiernach sind  alle Schwangeren ab dem zweiten Schwangerschaftsdrittel, Kinder, Jugendliche und Erwachsene mit erhöhter gesundheitlicher Gefährdung infolge eines Grundleidens (chronische Atemwegserkrankungen, Herz-, Kreislauferkrankungen, Diabetes und andere Stoffwechselerkrankungen, Multiple Sklerose oder Immunschwächekrankheiten), allerdings auch Bewohner von Alten- oder Pflegehheimen, sowie deren Mitarbeiter besonders gefährdet. Dieser Risikogruppe wird eine Schutzimpfung dringend empfohlen (siehe Abschnitt~\ref{sub:behandlung}). 

Influenza-Infektionen treten weltweit auf. In den Klimazonen der gemäßigten Breiten treten Grippewellen in den jeweiligen Wintermonaten auf. 
In Deutschland schwankt die Stärke der saisonalen Grippewellen. Es werden jährlich schätzungsweise 5 - 20\% der Bevölkerung infiziert. Die Arbeitsgemeinschaft Influenza (AGI) des Robert Koch Institutes schätzt pro Jahr zwischen einer und sieben Millionen Arztbesuche aufgrund von Influenza. Bei einer schweren Saison wie 2012/2013 wurden circa 30.000 Krankenhauseinweisungen und 20.000 Todesfälle geschätzt. Bei der darauffolgenden milden Saison wurden ,,nur'' 3.000 Krankenhauseinweisungen und keine signifikante Influenza-assozierte Übersterblichkeit nachgewiesen.\citef{RKI16, rki14, rki13} 

\subsection{Virologie}
\label{sub:virologie}

Es gibt drei Gattungen des Influenzavirus. Diese sind Orthomyxoviren, die sich in die drei Typen A, B und C einteilen lassen. Für den Menschen sind besonders Viren der saisonal auftretenden Typen A und B relevant. Die Viren bestehen aus Glykoproteine Hämagglutinin (HA) und Neuraminidase (NA), welche eine spikeartige Oberflächenstruktur bilden (siehe Abbildung~\ref{fig:h1n1}). 

\begin{figure}[h]
\begin{minipage}[t]{0.475\textwidth}
\centering
\includegraphics[width=0.98\textwidth]{h1n1.jpg}
\end{minipage}
\hfill
\begin{minipage}[t]{0.475\textwidth}
\centering
\includegraphics[width=0.98\textwidth]{h5n1.jpg}
\end{minipage}
\caption[Transmissions-Elektronenmikroskopie Aufnahmen Influenza A virus]{Transmissions-Elektronenmikroskopie (TEM) Aufnahmen. Ultradünnschnitt. Maßstab = 200 nm. \textbf{links:} FLUAV, Influenza A virus, Stamm Berlin H1N1. Primärvergrößerung x 98000. \textbf{rechts:} Influenza A virus (H5N1), Vogelgrippe.\protect\footnotemark}
\label{fig:h1n1}
\end{figure}
\footnotetext{Abbildung unverändert übernommen aus \cite{bannert}}

Im Allgemeinen werden die Influenza-A-Viren in erster Linie nach bestimmten, deutlich unterschiedlichen Oberflächeneigenschaften in Untertypen bzw. Subtypen eingeteilt. Dies geschieht nach dem Muster A(HxNx) oder A/Land/HxNx/Probe. Es gibt zur Zeit 18 bekannte HA- und 9 bekannte NA-Subtypen.\citef{RKI16} B-Typen haben keine Subtypen, es existieren jedoch zwei genetisch unterschiedliche Linien die auf der Unterscheidung der Oberflächenproteine basieren, die Yamagata-Linie und die Victoria-Linie.\citef{haas09} Während der B-Typ ausschließlich beim Menschen vorkommt, tritt der A-Typ auch bei Tieren, vor allem bei Vögeln und Säugetieren, auf. Üblicherweise sind die Symptome dieser Virusstämme auch nicht sehr ausgeprägt. 
2003 wurde in den Niederlanden 89 Personen mit dem krankheitserregenden Subtypen A(H7N7) infiziert. Davon hatten nur sieben Personen Influenza-artige Symptome. Bei den restlichen Personen wurden überwiegend Bindehautentzündungen diagnostiziert.\citef{fouchier03} Davon ausgenommen ist der Subtype A(H5N1), der zum ersten Mal in Hong Kong 1997 beim Menschen diagnostiziert wurde und bei uns gemeinhin als Vogelgrippe bekannt ist.\citef{xiyan99} 
2009 verdrängte ein neuer Subtyp A(H1N1)pdm09 den vorherigen A(H1N1) Subtyp. Diese globale Pandemie ist allgemein unter der Schweinegrippe bekannt.\citef{johansen09}
Es gibt vier verschiedene Arten, Influenza Viren zu übertragen:
\begin{enumerate*}[label=(\roman*)]
	\item Übertragung durch direkten physischen Kontakt;
	\item Übertragung durch zwischen Wirt und Empfänger liegende leblose Objekte (Geländer, Türklinken);
	\item Übertragung durch Tröpfcheninfektion durch Husten oder Niesen;
	\item Übertragung durch in der Luft befindliche Aerosole.
\end{enumerate*}

\subsection{Erkennen von Influenzaausbrüchen}
\label{sub:erkennen_von_influenzaausbrüchen}

Mit der Arbeitsgemeinschaft Influenza (AGI) existiert in Deutschland ein seit Jahren etabliertes System zur bundesweiten Erfassung Influenza-bedingter Erkrankungen, mit dem die jahreszeitliche Aktivität der Influenza zeitnah berichtet und Daten zur Krankheitslast erhoben werden. Die AGI übernimmt dabei zwei Aufgaben. Die virologische Überwachung, die sich mit den auftretenden Subtypen befasst, sowie die syndromische Überwachung, die sich mit der Überwachung akuter Atemwegserkrankungen (ARE) befasst. Dabei melden 500 - 700 primärversorgende Ärzte wöchentlich die Zahl ihrer Patienten mit ARE. Diese werden in 7 Altersgruppen unterteilt. Es werden zwei verschiedene Indizes gemeldet. Die Konsultationsinzidenz und der Praxisindex. Die folgenden Definitionen stammen aus \cite{schweiger12}. Die Konsulationsinzidenz gibt Aufschluß über Neuerkrankungen und darf nur gemeldet werden, wenn der Patient in den vorherigen zwei Wochen keinen Arzt wegen ARE aufgesucht hat. Er wird wird angegeben als Anzahl der sich in den Arztpraxen vorstellenden Patienten mit ARE pro 100.000 Einwohner der jeweiligen Altersgruppe. Der Praxisindex stellt hingegen die über alle Praxen gemittelte relative Abweichung der beobachteten Zahl an ARE und der Zahl an ARE je 100 Praxiskontakte gegenüber einem für jede Sentinelpraxis ermittelten „Normalniveau“ dar. In Abbildung~\ref{fig:praxisindex14} ist der Praxisindex der Grippewellen 2009 - 2014 aufgetragen. Gemäß Definition der AGI beginnt die Grippewelle bundesweit, wenn die Grenze für das untere 95\%-Konfidenzintervall der geschätzten Influenza-Positivenrate in 2 aufeinanderfolgenden Kalenderwochen um 10\% überschreitet wird mit der ersten Woche dieses Zeitraums.

\begin{figure}[h]
\centering
\includegraphics[width=0.98\textwidth]{praxisindex_09-14.png}
\caption[Praxisindex im Vergleich für die jeweiligen Wintersaisons 2009/10 bis 2013/14]{Praxisindex im Vergleich für die jeweiligen Wintersaisons 2009/10 bis 2013/14 (40. KW bis 15. KW des Folgejahres).\protect\footnotemark}
\label{fig:praxisindex14}
\end{figure}
\footnotetext{Abbildung unverändert übernommen aus \cite{rki14}}

\subsection{Behandlung}
\label{sub:behandlung}

Die meisten Patienten mittleren Alters und aber auch junge Erwachsene benötigen keine spezielle Behandlung. Bei älteren Patienten und allgemein Personen der Risikogruppen (siehe Abschnitt~\ref{sub:krankheitsverlauf}) scheint eine Behandlung mit antiviralen Medikamenten aussichtsreich. Gegen Grippeviren existieren Neuramidasehemmer und M2-Membranpro\-te\-in\-hem\-mer. Neuramidasehemmer sind gegen die meisten Subtypen einsetzbar.\citef{tumpey02} Sie haben kleine, unspezifische Auswirkungen auf die Linderung der Influenzasymptome.\citef{jefferson14} Für zukünftige schwere Grippewellen wird ihnen eine Bedeutung vorhergesagt, vor allem falls Impfstoffe gegen einen neuen Virusstamm noch nicht entwickelt wurden.\citef{hoffmann16} M2-Membranproteinhemmer haben vor allem in der Prophylaxe und der Behandlung während einer Influenza-Pandemie eine Bedeutung. Es wird allerdings nicht oft eingesetzt aufgrund von Resistenzen, die die Influenzaviren aufbauen.\citef{monto03} 

Um der Ansteckung mit Influenzaviren vorzubeugen rät die ständige Impfkommission vor allem der Risikogruppe zur Grippeimpfung. Da sich die zirkulierenden Influenzaviren kontinuierlich genetisch verändern, spricht die Weltgesundheitsorganisation (WHO) jedes Jahr eine Empfehlung für die Zusammensetzung des jeweils aktuellen Impfstoffes aus. Nach einer Impfung besteht nach circa 14 Tagen Impfschutz. Aufgrund der Annahmen, die für die Empfehlung der im Impfstoff enthaltenen Virenstämme getroffen werden, variiert auch dessen Wirksamkeit. Ein Beispiel einer historisch schlechten Wirksamkeit wird für den A(H3N2)-Stamm in der Saison 2014/2015 beschrieben (unter 10\%).\citef{skowronski16} Die vorläufigen Ergebnisse der Impf-Effektivität für die Grippesaison 2016/2017 beträgt 41\%.\citef{reuss17} 

\section{Meteorologische Größen} % (fold)
\label{sec:meteorologische_größen}

Für diese Arbeit werden einige Grundgrößen der Feuchte in der Meteorologie gebraucht. Im folgenden Abschnitt werden Definitionen gemäß der WMO gegeben.\citef{wmo14}

Betrachtet man ein Medium in einem abgeschlossenen System, werden zwischen dem flüssigem Aggregatzustand (z.B. Wasser) einige Moleküle an dessen Oberfläche in die gasförmige Phase übergehen. Je höher dabei die Temperatur des Mediums ist, desto mehr thermodynamische Energie besitzen die Moleküle und desto mehr Moleküle treten in die gasförmige Phase über. Der Druck dieser Moleküle wird \textit{Dampfdruck} $e$ genannt. Die Abhängigkeit der Temperatur ist in der idealen Gasgleichung erkennbar. 

\begin{align}
	e = \rho_{v}R_{v}T,
\end{align}

wobei $\rho_{v}$ die Wasserdampfdichte und $R_{v}$ die spezifische Gaskonstante für Wasserdampf ist. 

Die Wasserdampfdichte wird auch \textit{absolute Feuchte} (AH) genannt. 

\begin{align}
	\rho_{v}=\frac{m_{v}}{V}, 
\end{align}
wobei $m_{v}$ die Masse des Wasserdampfes und $V$ das Volumen des Luftpaketes ist. Sie stellt ein Maß für den Wasserdampfgehalt der Luft dar und gibt an wie viel Masse an Wasserdampf in einem Kubikmeter Luft enthalten ist. 

Das \textit{Mischungsverhältnis} $m$ ist das Verhältnis der Masse des Wasserdampfes pro kg trockener Luft

\begin{align}
	m = \frac{m_{v}}{m_{a}},
\end{align}
wobei $m_{a}$ die Masse der trockenen Luft ist.
Die \textit{spezifische Feuchte} gibt das Verhältnis zwischen der Masse des Wasserdampfes pro kg feuchter Luft an. 

\begin{align}
	q = \frac{m_{v}}{m_{a} + m_{v}}
\end{align}
Über das Daltonsche Gesetz des Partialdrucks\citef{dalton89} lässt sich $q$ in ausreichender Näherung über den Luftdruck und den Dampfdruck bestimmen.

\begin{align}
	q = 0.622 \frac{e}{p-e} \approx 0.622\frac{e}{p}, \ \text{mit} \ \ e \ll p \label{eq:dalton}
\end{align}
Die \textit{relative Feuchte} gibt den Grad der Sättigung des Wasserdampfes in Luft an. Sie wird berechnet aus dem Dampfdruck und dem Sättigungsdampfdruck. Der \textit{Sättigungsdampfdruck} $e_{s}$ lässt sich über die Clausius-Clapeyron Gleichung ausrechnen.

\begin{align}
	e_{s} = e_{0} \exp{ \left[\frac{L}{R_{v}} \left( \frac{1}{T_{0}} - \frac{1}{T} \right)  \right]}, 
\end{align}
wobei $e_{0}$ der Dampfdruck bei einer bestimmten Temperatur $T_{0}$, und $L$ die latente Wärme ist. Als latente Wärme wird die Energiemenge bezeichnet, die ein Medium benötigt beziehungsweise abgibt, um von einem Aggregatzustand in einen Anderen zu gelangen.

Die relative Feuchte $RH$ ist der Grad der Sättigung, oder

\begin{align}
	RH = \frac{e}{e_{s}} \cdot 100 \ \%.
\end{align}
Da sich der Sättigungsdampfdruck mit steigender Temperatur verringert, kann ein wärmeres Luftpaket mehr Wasserdampf enthalten als ein kühleres. Oder anders ausgedrückt, kühlt man ein Luftpaket ab, steigt der Dampfdruck des enthaltenen Wasserdampfes.

\section{Influenzaviren und die Feuchte} % (fold)
\label{sec:umwelteinflüsse_auf_influenza_viren}

\subsection{Relative Feuchte} % (fold)
\label{sub:relativer_Feuchte}

In frühen Studien wurde mehrmals gezeigt, dass Viren, die in einem Aerosol vorliegen, je nach Feuchte unterschiedlich lange überleben können. Während die Ergebnisse im Bezug auf eine geringe und moderate Feuchte übereinstimmen, weichen sie für eine hohe Feuchte ab. Die Viabilität (Überledensdauer) sinkt demnach mit steigender Feuchte.\citef{schaffer76,hemmes60} Aktuelle Forschungsergebnisse liefern eine Erklärung für die abweichenden Ergebnisse bei hoher Feuchte, indem sie eine Abhängigkeit von Salz und Proteinkonzentrationen in Tröpfchen zeigen. Demnach gibt es drei Zustände verschiedener $RH$ der Viabilität von Influenza Typ-A-Viren (siehe Tabelle \ref{tab:iva_viabilität}).

\begin{table}[t]
\begin{center}
\begin{small}
\resizebox{\textwidth}{!}{\begin{tabular}{l | c | l}
\toprule
Umgebung 	& 	relative Feuchte 	&  	Viabilität \\
\midrule 
\textit{physiologische Umgebung} 	& 	$\sim 100\% \ RH$ 	& 	hoch \\
\textit{konzentrierte Umgebung} 	&	$50-99\% \ RH$ 	&   niedrig (abhängig vom Medium) \\
\textit{trockene Umgebung} 			& 	$<50\% \ RH$ 		&	hoch \\
\bottomrule
\end{tabular}}
\end{small}
\end{center}
\caption{Beziehung zwischen relativer Feuchte und
der Viabilität von Influenzaviren (A-Typ)}
\label{tab:iva_viabilität}
\end{table}

In einem Laborexperiment mit Meerschweinchen wurden diese als Wirte verwendet um die Abhängigkeit der Übertragung von Influenzaviren, $RH$ und Temperatur auf nicht infizierte Artgenossen zu zeigen. Für 20\degree C ist die Übertragungsrate am höchsten bei niedriger RH. Für mittlere RH nimmt die Übertragungsrate ab. 
\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{Chapter2/Figs/iav-transmission.png}
  \end{center}
  \caption[Übertragung von Influenzaviren in Abhängigkeit von T und RH]{Verschieden effektive Übertragungen von Influenzaviren in Abhängigkeit der RH. Modelberechnungen aus Studien mit Meerschweinchen. \textbf{gestrichelte Linie:} 20\degree C. \textbf{durchgezogene Linie:} 5\degree C.\protect\footnotemark}
  \label{fig:iav-transmission}
\end{wrapfigure}
\footnotetext{Abbildung unverändert übernommen aus \cite{lowen06}}
Erhöht man RH weiter, nimmt die Viabilität und damit die Übertragungsrate weiter zu, bis sehr hohe RH Werte die Tröpchen so groß werden lassen, dass sie eher zu Boden fallen, anstatt den Weg durch die Luft zu einem anderen Wirt zu finden.\citef{lowen06} Für 5\degree C ist die Übertragungsrate effizienter als für 20\degree C. Sie sinkt für höhere RH Werte gegen 50\% (siehe Abbildung \ref{fig:iav-transmission}).

\subsection{Absolute Feuchte} % (fold)
\label{sub:absolute_feuchte}

Da die RH eng mit der Temperatur verbunden ist (siehe Abschnitt \ref{sec:meteorologische_größen}), wurde untersucht ob sich die Ergebnisse auch auf die absolute Feuchte (AH) übertragen lassen. 

Um den Einfluss der absoluten Feuchte auf Viabilität und Übertragungsrate der Viren zu bestimmen, wurde die absolute Feuchte aus den Daten der Meerschweinchenstudie berechnet.\citef{shamann09} Dabei wurde die AH in Form des Dampfdrucks von Wasserdampf über die Temperatur und RH berechnet. Die Autoren kamen zu dem Ergebnis, dass die absolute Feuchte statistisch signifikanter im Gegensatz zur relativen Feuchte ist. 50\% der Variabilität der Übertragungsrate und 90\% der Variabilität der Überlebensdauer ließen sich mit der absoluten Feuchte erklären. Dagegen stehen 12\% und 36\% der relativen Feuchte. Daher wird vermutetet, dass die AH einen stärkeren Einfluss als Temperatur oder RH auf die Übertragungsrate der Viren in der Luft hat. Es zeigte sich eine stark nicht-lineare Abhängigkeit von AH und der Überlebensdauer der Viren. Auch für den Fall der Viabilität sind Anzeichen für eine nicht-lineare Abhängigkeit vorhanden (siehe Abbildung \ref{fig:iav-transmission}).

\begin{figure}[h]
\centering
\includegraphics[width=0.98\textwidth]{Chapter2/Figs/via-trans-ah.png}
\caption[Übertragunsrate von Influenza-A-Viren]{\textbf{links:} Regression der Übertragungsrate von Influenza-A-Viren zwischen Meerschweinchen. \textit{gestrichelt:} log(Percent Transmission). \textbf{rechts:} Lineare Regression der Viabilität logarithmisch aufgetragen.\protect\footnotemark}
\label{fig:ah}
\end{figure}
\footnotetext{Abbildung unverändert übernommen aus \cite{shamann09}}

\nomenclature{HA}{Hämagglutinin}
\nomenclature{NA}{Neuraminidase}
\nomenclature{RKI}{Robert Koch Institut}
\nomenclature{AGI}{Arbeitsgemeinschaft Influenza}
\nomenclature{ARE}{akute Atemwegserkrankungen}
\nomenclature{WHO}{Weltgesundheitsorganisation}
\nomenclature{WMO}{World Meteorological Organization}
\nomenclature{RH}{relative Feuchte}
\nomenclature{AH}{absolute Feuchte}
\nomenclature{q}{spezifische Feuchte}
\nomenclature{T}{Temperatur}