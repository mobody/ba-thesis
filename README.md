## Author
*   Moritz Schoenegg

_This is a fork of [Krishna Kumar's Cambridge template](https://github.com/kks32/phd-thesis-template)_

## Building the thesis - XeLaTeX

This template supports `XeLaTeX` compilation chain. To generate  PDF run

    latexmk -xelatex thesis.tex
    makeindex thesis.nlo -s nomencl.ist -o thesis.nls
    latexmk -xelatex -g thesis.tex
-------------------------------------------------------------------------------

## Get the most recent build [here](https://gitlab.com/mobody/ba-thesis/raw/master/thesis.pdf)